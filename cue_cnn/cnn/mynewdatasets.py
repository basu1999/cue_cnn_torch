import re
import os
import random
from torchtext.legacy import data
import torchtext.datasets as datasets
from torchtext import vocab
import pandas as pd

class MR(data.Dataset):

    @staticmethod
    def sort_key(ex):
        return len(ex.text)

    def __init__(self, text_field, label_field, user_field, file_path, examples=None, **kwargs):
        def clean_str(string):
            """
            Tokenization/string cleaning for all datasets except for SST.
            Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
            """
            string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
            string = re.sub(r"\'s", " \'s", string)
            string = re.sub(r"\'ve", " \'ve", string)
            string = re.sub(r"n\'t", " n\'t", string)
            string = re.sub(r"\'re", " \'re", string)
            string = re.sub(r"\'d", " \'d", string)
            string = re.sub(r"\'ll", " \'ll", string)
            string = re.sub(r",", " , ", string)
            string = re.sub(r"!", " ! ", string)
            string = re.sub(r"\(", " \( ", string)
            string = re.sub(r"\)", " \) ", string)
            string = re.sub(r"\?", " \? ", string)
            string = re.sub(r"\s{2,}", " ", string)
            return string.strip()

        text_field.tokenize = lambda x: clean_str(x).split()
        fields = [('text', text_field), ('label', label_field), ('user', user_field)]
        if examples is None:
            # Load the dataset
            examples = []
            with open(file_path, encoding='utf-8') as f:
                for line in f:
                    label, user, text = line.strip().split(',')
                    examples.append(data.Example.fromlist([text, label, user], fields))
        super(MR, self).__init__(examples, fields, **kwargs)

    @classmethod
    def splits(cls, text_field, label_field, user_field, file_path, test_ratio=0.1, shuffle=True, **kwargs):
        examples = cls(text_field, label_field, user_field, file_path, **kwargs).examples
        if shuffle:
            random.shuffle(examples)
        split_index = int(len(examples) * (1 - test_ratio))
        train_examples = examples[:split_index]
        test_examples = examples[split_index:]
        dev_ratio = 0.1
        dev_index = int(len(train_examples) * (1 - dev_ratio))
        dev_examples = train_examples[dev_index:]
        train_examples = train_examples[:dev_index]
        return cls(text_field, label_field, user_field, file_path, examples=train_examples, **kwargs), \
               cls(text_field, label_field, user_field, file_path, examples=dev_examples, **kwargs), \
               cls(text_field, label_field, user_field, file_path, examples=test_examples, **kwargs)

