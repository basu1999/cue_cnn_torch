# load Sarcasm dataset
#import mydatasets
import mynewdatasets as mydatasets
import torch
import torchtext.legacy.data as data
from torchtext import vocab
import re

def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip()
batch_size = 128
def dataloader(text_field, label_field, user_field, file_path, args, wdir=None, u2vdir=None, **kargs):
    print("Text field= ",text_field)
    print("Label field= ",label_field)
    print("User field= ",user_field)
    print("File path= ",file_path)
    print("Args= ",args)
    print("Wdir= ",wdir)
    train_data, dev_data, test_data = mydatasets.MR.splits(text_field, label_field, user_field, file_path)
    print("Test data= ",test_data)

    if args.pretrained_embed_words:
        print("if word embedding")
        custom_embed = vocab.Vectors(name=wdir, max_vectors=100000)
        text_field.build_vocab(train_data, dev_data, test_data, vectors=custom_embed)
        print("Text field vocab= ",text_field.vocab)
    else:
        print("Else word embedding")
        text_field.build_vocab(train_data, dev_data, test_data)

    if args.pretrained_embed_users:
        print("if user embedding")
        custom_embed_u = vocab.Vectors(name=u2vdir, max_vectors=8000)
        user_field.build_vocab(train_data, dev_data, test_data, vectors=custom_embed_u)
    else:
        print("Else user embedding")
        user_field.build_vocab(train_data, dev_data, test_data)

    label_field.build_vocab(train_data, dev_data, test_data)

    train_iter, dev_iter, test_iter = data.Iterator.splits(
        (train_data, dev_data, test_data),
        batch_sizes=(batch_size, len(dev_data), len(test_data)),
        **kargs)
    return train_iter, dev_iter, test_iter
